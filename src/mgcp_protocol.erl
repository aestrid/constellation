%%%-------------------------------------------------------------------
%%% @author Astrid Smith <>
%%% @copyright 2017, Astrid Smith
%%% @doc
%%%
%%% This is an protocol process, which communicates with a single MGCP
%%% gateway device.
%%%
%%% @end
%%% Created : 25 Apr 2017 by Astrid Smith <>
%%%-------------------------------------------------------------------
-module(mgcp_protocol).

-behaviour(gen_server).

-include("include/mgcp.hrl").

%% API
-export([start_link/1
        ,make_request/2
        ,inbound/2
        ]).

%% MGCP API
-export([endpoint_configuration/0
        ,notification_request/3
        %,notify/0 % gateway-originated only
        ,create_connection/0
        ,modify_connection/0
        ,delete_connection/0
        ,audit_endpoint/3
        ,audit_connection/0
        %,restart_in_progress/0 % gateway-originated only
        ]).

%% gen_server callbacks
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
         terminate/2, code_change/3]).

-define(SERVER, ?MODULE).

-record(state, {address
               ,pending
               ,socket
               ,next_txid
               ,agent
               }).

-record(tx, {starttime
            ,request :: #mgcp_msg{}
            ,client % for use with gen_server:reply/2
            }).

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%%
%% @spec start_link() -> {ok, Pid} | ignore | {error, Error}
%% @end
%%--------------------------------------------------------------------
start_link(Address) ->
    gen_server:start_link({global, {protocol, Address}}, ?MODULE, [Address], []).

%%--------------------------------------------------------------------
%% @doc
%% Make a request of the client.
%% @end
%%--------------------------------------------------------------------
-spec make_request(Pid :: pid(), Request::#mgcp_msg{}) -> _Any.
make_request(Pid, Request) ->
    % ??? is this correct?
    gen_server:call(Pid, {send, Request}).

%%--------------------------------------------------------------------
%% @doc
%%
%% A packet has been received, forward it to the right protocol
%% process.
%%
%% @end
%%--------------------------------------------------------------------
inbound(Address, Packet) ->
    Pid = case global:whereis_name({protocol, Address}) of
              undefined -> dg_sup:add_device(Address),
                           {global, {protocol, Address}};
              Pid1 -> Pid1
          end,
    gen_server:cast(Pid, {inbound, Packet}).
   
%%--------------------------------------------------------------------
%% @doc
%%
%% @spec audit_endpoint(Address, EndpointID, RequestedInfo) -> {ok, Info} | {error, Error}
%% @end
%%--------------------------------------------------------------------
audit_endpoint(Address, EndpointID, RequestedInfo) ->
    gen_server:call({global, {protocol, Address}}, 
                    {audit_endpoint, EndpointID, RequestedInfo}).

notification_request(Address, EndpointID, RequestedEvents) -> 
    gen_server:call({global, {protocol, Address}},
                    {notification_request, EndpointID, RequestedEvents}).

create_connection() -> 
    error.
modify_connection() ->
    error.
delete_connection() ->
    error.
audit_connection() ->
    error.
endpoint_configuration() ->
    error.

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%%
%% @spec init(Args) -> {ok, State} |
%%                     {ok, State, Timeout} |
%%                     ignore |
%%                     {stop, Reason}
%% @end
%%--------------------------------------------------------------------

init([Address]) ->
    process_flag(trap_exit, true),
    {ok, #state{address = Address
               ,pending = maps:new()
               ,next_txid = 1
               }};
init(_Args) ->
    init([undefined]).


%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%%
%% @spec handle_call(Request, From, State) ->
%%                                   {reply, Reply, State} |
%%                                   {reply, Reply, State, Timeout} |
%%                                   {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, Reply, State} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_call({send, Msg, Addr}, From, State) ->
    {ok, NewState} = prepare_message(Msg, From, State),
    {noreply, NewState};
handle_call({audit_endpoint, EndpointID, RequestedInfo}, From, State) ->
    {ok, NewState} = prepare_message(
                       #mgcp_msg{topline = #mgcp_cmd{verb = audit_endpoint,
                                                     endpoint = EndpointID},
                                 headers = [#mgcp_hdr{name = requested_info, 
                                                      value = RequestedInfo}],
                                 body = []},
                       From, State),
    {noreply, NewState};
handle_call(_Request, _From, State) ->
    Reply = ok,
    {reply, Reply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%%
%% @spec handle_cast(Msg, State) -> {noreply, State} |
%%                                  {noreply, State, Timeout} |
%%                                  {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_cast({inbound, Packet}, State) ->
    process_message(mgcp_parser:decode(Packet), State);
handle_cast(_Msg, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State) -> {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_info(_Info, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
terminate(_Reason, _State) ->
    ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Extra) -> {ok, NewState}
%% @end
%%--------------------------------------------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%%
%% Process an incoming message from the network.  If it's a request,
%% process it directly. or send it to the correct registered callback.
%%
%% If it's a response, find the calling function, mark the matching
%% request as completed, and send the response to the requesting
%% process.
%%
%% @end
%%--------------------------------------------------------------------
process_message(Msg = #mgcp_msg{topline=#mgcp_cmd{verb=Verb, tid=Tid}}, State) ->
    %% TODO: we need to deal with commands from the far end
    {ok, Reply} = mgw_agent:Verb(State#state.address, Msg),

    %% make sure the transaction ID is the same
    Topline = Reply#mgcp_msg.topline,
    ToplineMunged = Topline#mgcp_resp{tid=Tid},
    ReplyMunged = Reply#mgcp_msg{topline=ToplineMunged},
    io:format("process_message: have outbound ready ~p~n", [ReplyMunged]),
    {ok, NewState} = prepare_message(ReplyMunged, self, State),
    {noreply, NewState};
% incoming reply, direct to proper location
process_message(Msg = #mgcp_msg{topline=#mgcp_resp{}}, State) ->
    Pending = State#state.pending,
    Tid = Msg#mgcp_msg.topline#mgcp_resp.tid,
    io:format("tid = ~p~n", [Tid]),
    Tx = maps:get(Tid, Pending),
    NewPending = maps:remove(Tid, Pending),
    gen_server:reply(Tx#tx.client, {ok, Msg}),
    {ok, State#state{pending = NewPending}};
process_message(_, State) ->
    {ok, State}.


%%--------------------------------------------------------------------
%% @doc
%%
%% Prepare an outgoing message to the network.  If it's a request,
%% assign a transaction ID and hang on to the message for
%% retransmission, and then send it.
%%
%% If it's a response, just send it.
%%
%% @end
%%--------------------------------------------------------------------
prepare_message(Msg = #mgcp_msg{topline=#mgcp_cmd{}}, From, State) ->
    {ok, Txid, NewState} = assign_txid(State),
    NewTopline = Msg#mgcp_msg.topline#mgcp_cmd{tid=Txid},
    NewMsg = Msg#mgcp_msg{topline = NewTopline},
    Tx = #tx{starttime = erlang:monotonic_time(), request = NewMsg, client = From},
    Bytes = mgcp_parser:encode(NewMsg),
    udp_router:send(State#state.address, Bytes),
    NewPending = maps:put(Txid, Tx, State#state.pending),
    {ok, State#state{pending = NewPending}};
prepare_message(Msg = #mgcp_msg{topline=#mgcp_resp{}}, _From, State) ->
    Bytes = mgcp_parser:encode(Msg),
    udp_router:send(State#state.address, Bytes),
    {ok, State}.

% TODO: do the correct thing if the txid wraps around past a
% gazillion or whatever.
assign_txid(State) ->
    Txid = State#state.next_txid,
    NewState = State#state{next_txid = Txid + 1},
    {ok, Txid, NewState}.


% TODO when we talk to the gateway, add a bunch of endpoint
% supervisors (one for each endpoint)
%
%    Endpoint = {'endpoint', {ep_sup, start_link, []},
%				permanent, Shutdown, supervisor},




