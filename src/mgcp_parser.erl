-module(mgcp_parser).

-export([encode/1
        ,decode/1
        ]).

-export([get_header/3]).

-export([encode_header/1]).

-include("include/mgcp.hrl").

-define(verbs, [{endpoint_configuration, <<"EPCF">>}
               ,{create_connection, <<"CRCX">>}
               ,{modify_connection, <<"MDCX">>}
               ,{delete_connection, <<"DLCX">>}
               ,{notification_request, <<"RQNT">>}
               ,{notify, <<"NTFY">>}
               ,{audit_endpoint, <<"AUEP">>}
               ,{audit_connection, <<"AUCX">>}
               ,{restart_in_progress, <<"RSIP">>}
               ]).

% rfc 3435 sec 3.2.2
-define(parameters, [{bearer_information, <<"B">>}
                    ,{call_id, <<"C">>}
                    ,{capabilities, <<"A">>}
                    ,{connection_id, <<"I">>}
                    ,{connection_mode, <<"M">>}
                    ,{connection_parameters, <<"P">>}
                    ,{detect_events, <<"T">>}
                    ,{digit_map, <<"D">>}
                    ,{event_states, <<"ES">>}
                    ,{local_connection_options, <<"L">>}
                    ,{max_mgcp_datagram, <<"MD">>}
                    ,{notified_entity, <<"N">>}
                    ,{observed_events, <<"O">>}
                    ,{package_list, <<"PL">>}
                    ,{quarantine_handling, <<"Q">>}
                    ,{reason_code, <<"E">>}
                    ,{requested_events, <<"R">>}
                    ,{requested_info, <<"F">>}
                    ,{request_identifier, <<"X">>}
                    ,{response_ack, <<"K">>}
                    ,{restart_delay, <<"RD">>}
                    ,{restart_method, <<"RM">>}
                    ,{second_connection_id, <<"I2">>}
                    ,{signal_requests, <<"S">>}
                    ,{specific_endpoint_id, <<"Z">>}
                    ,{remote_connection_descriptor, <<"RC">>}
                    ,{local_connection_descriptor, <<"LC">>}
                    ]).

-define(packages, [{generic_media, <<"G">>}
                  ,{dtmf, <<"D">>}
                  ,{trunk, <<"T">>}
                  ,{line, <<"L">>}
                  ,{handset, <<"H">>}
                  ,{supplementary_services, <<"SST">>}
                  ,{digit_map_extension, <<"DM1">>}
                  ,{signal_list, <<"SL">>}
                  ,{media_format, <<"FM">>}
                  ,{rtp, <<"R">>}
                  ,{resource_reservation, <<"RES">>}
                  ,{announcement_server, <<"A">>}
                  ,{script, <<"Script">>}
                  ]).

-define(on_off, [{on, <<"on">>}
                ,{off, <<"off">>}
                ]).

-define(restart_methods, [{graceful, <<"graceful">>}
                         ,{forced, <<"forced">>}
                         ,{restart, <<"restart">>}
                         ,{disconnected, <<"disconnected">>}
                         ,{cancel_graceful, <<"cancel-graceful">>}
                         ]).

-spec encode(Msg::#mgcp_msg{}) -> binary().
encode(Msg) ->
    io:format("encoding ~p~n", [Msg]),
    iolist_to_binary(
      [encode_topline(Msg#mgcp_msg.topline)
      ,"\n"
      ,lists:join("\n", lists:map(fun (H) -> encode_header(H) end, Msg#mgcp_msg.headers))
      ,"\n\n"
      ,Msg#mgcp_msg.body
      ]).

-spec decode(Msg::binary()) -> #mgcp_msg{}.
decode(Msg) ->
    [Head | Body] = binary:split(Msg, <<"\n\n">>),
    [Topline | Headers] = binary:split(Head, <<"\n">>, [global, trim_all]),
    #mgcp_msg{topline = decode_topline(Topline)
             ,headers = lists:map(fun(H) -> decode_header(H) end, Headers)
             ,body = decode_body(Body)}.

encode_topline(Cmd = #mgcp_cmd{}) ->
    lists:join(" ", [encode_verb(Cmd#mgcp_cmd.verb)
                    ,integer_to_list(Cmd#mgcp_cmd.tid)
                    ,lists:join(" ", Cmd#mgcp_cmd.endpoint)
                    ,encode_version(Cmd#mgcp_cmd.version)
                    ]);
encode_topline(Cmd = #mgcp_resp{}) ->
    lists:join(" ", [integer_to_list(Cmd#mgcp_resp.code)
                     %% todo: package identifier, if defined
                    ,integer_to_list(Cmd#mgcp_resp.tid)
                    ,Cmd#mgcp_resp.commentary
                    ]).

decode_topline(Line) when is_binary(Line) ->
    Linesplit = binary:split(Line, [<<" ">>, <<"\t">>], [global, trim_all]),
    decode_topline(Linesplit);
decode_topline([Verb, TidStr, Endpoint | Version]) when size(Verb) == 4 ->
    {Tid, []} = string:to_integer(binary:bin_to_list(TidStr)),
    #mgcp_cmd{verb = decode_verb(Verb)
             ,tid = Tid
             ,endpoint = binary:split(Endpoint, <<"@">>)
              %% ugh, I wish I didn't have to paste the version string
              %% back together.  But in their infinite wisdom, the
              %% goons at Cisco who designed MGCP used a space instead
              %% of a slash like in HTTP.
             ,version = decode_version(iolist_to_binary(lists:join(" ", Version)))
             };
decode_topline([CodeStr, TidStr | Commentary]) when size(CodeStr) == 3 ->
    {Tid, []} = string:to_integer(binary:bin_to_list(TidStr)),
    {Code, []} = string:to_integer(binary:bin_to_list(CodeStr)),
    #mgcp_resp{code = Code
              ,tid = Tid
              ,commentary = Commentary
              }.

encode_verb(Verb) ->
    encode_thing(Verb, ?verbs).
decode_verb(Verb) ->
    decode_thing(Verb, ?verbs).

encode_thing(Verb, Words) ->
    {value, {_Atom, Bin}} = lists:keysearch(Verb, 1, Words),
    Bin.
decode_thing(Verb, Words) ->
    {value, {Atom, _Bin}} = lists:keysearch(Verb, 2, Words),
    Atom.

% No protocol version other than 1.0 has ever been defined ...
encode_version(1.0) -> "MGCP 1.0";
encode_version(_) -> "MGCP 1.0".
decode_version(<<"MGCP 1.0">>) -> 1.0 .

encode_header(Hdr) ->
    {value, {_Atom, Bin}} = lists:keysearch(Hdr#mgcp_hdr.name, 1, ?parameters),
    [ Bin , ": ", Hdr#mgcp_hdr.value ].

decode_header(Hdr) when is_binary(Hdr) ->
    [Name, Value] = binary:split(Hdr, <<":">>, [trim_all]),
    Atom = case lists:keysearch(Name, 2, ?parameters) of
               {value, {A, _B}} -> A;
               false -> Name
           end,
    decode_header(#mgcp_hdr{name = Atom
                           ,value = Value
                           });
decode_header(Hdr = #mgcp_hdr{name = capabilities}) ->
    Hdr#mgcp_hdr{value = decode_header_capabilities(
                           binary:split(Hdr#mgcp_hdr.value, <<",">>, [global, trim_all]))
                };
decode_header(Hdr = #mgcp_hdr{name = restart_method}) -> 
    Hdr#mgcp_hdr{value = decode_thing(Hdr#mgcp_hdr.value, ?restart_methods)};
decode_header(Hdr = #mgcp_hdr{name = restart_delay}) ->
    Hdr#mgcp_hdr{value = decode_int(Hdr#mgcp_hdr.value)};
decode_header(Hdr = #mgcp_hdr{}) ->
    Hdr.

decode_int(Bin) ->
    {Int, Rest} = string:to_integer(binary:bin_to_list(Bin)),
    Int.

decode_header_capabilities(Caps) ->
    lists:map(fun(H) -> decode_header_capabilities_single(H) end, Caps).

decode_header_capabilities_single(<<" ", Rest/bytes>>) ->
    decode_header_capabilities_single(Rest);
decode_header_capabilities_single(<<"a:", Rest/bytes>>) ->
    {allowed_codecs, binary:split(Rest, <<";">>, [global])};
decode_header_capabilities_single(<<"b:", Rest/bytes>>) ->
    {bandwidth, Rest};
decode_header_capabilities_single(<<"p:", Rest/bytes>>) ->
    {packetization_period, Rest};
decode_header_capabilities_single(<<"t:", Rest/bytes>>) ->
    {type_of_service, Rest};
decode_header_capabilities_single(<<"e:", Rest/bytes>>) ->
    {echo_cancellation, decode_thing(Rest, ?on_off)};
decode_header_capabilities_single(<<"gc:", Rest/bytes>>) ->
    {gain_control, Rest};
decode_header_capabilities_single(<<"s:", Rest/bytes>>) ->
    {silence_suppression, decode_thing(Rest, ?on_off)};
decode_header_capabilities_single(<<"r:", Rest/bytes>>) ->
    {resource_reservation, Rest};
decode_header_capabilities_single(<<"k:", Rest/bytes>>) ->
    {encryption_key, Rest};
decode_header_capabilities_single(<<"nt:", Rest/bytes>>) ->
    {network_type, Rest};
decode_header_capabilities_single(<<"v:", Rest/bytes>>) ->
    {packages, lists:map(fun(Package) -> decode_thing(Package, ?packages) end,
                         binary:split(Rest, <<";">>, [global]))};
decode_header_capabilities_single(<<"m:", Rest/bytes>>) ->
    {modes, binary:split(Rest, <<";">>, [global])};
decode_header_capabilities_single(<<>>) ->
    {};
decode_header_capabilities_single(Bin) ->
    {Bin}.

decode_body([]) -> <<>>;
decode_body([Body]) when is_binary(Body) -> Body;
decode_body(Body) when is_binary(Body) -> Body.

get_header(Msg = #mgcp_msg{headers = H}, Name, Default) ->
    R = lists:keysearch(Name, 2, H),
    case R of
        {value, V} -> V;
        false -> Default
    end.
